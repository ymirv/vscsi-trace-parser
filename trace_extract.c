#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>

#include "trace_format.h"


int main(int argc, char **argv)
{
	struct stat st;	
	char *filename = NULL;
	int f = -1;
	void *mem = NULL;
	int done = 0;
	int delta = 0;
	void (*fptr)(FILE *, void *) = NULL;
	
	if (argc < 2)
	{
		fprintf (stderr, "Usage: %s <tracefile>\n", argv[0]);
		return 0;
	}
	
	filename = argv[1];
	if ( (f = open (filename, O_RDONLY)) < 0)
	{
		fprintf (stderr, "Unable to open '%s'\n", filename);
		return -1;
	}

	if ( (fstat (f, &st)) < 0)	
	{
		close (f);
		fprintf (stderr, "Unable to fstat '%s'\n", filename);
		return -1;
	}

	/* Version 2 records are the bigger of the two */
	if (st.st_size < sizeof(trace_v2_record_t) * MAX_TEST)
	{
		close (f);
		fprintf (stderr, "File too small, unable to read header.\n");
		return -1;
	}

	if ( (mem = mmap (NULL, st.st_size, PROT_READ, MAP_PRIVATE, f, 0)) == MAP_FAILED)
	{
		close (f);
		fprintf (stderr, "Unable to allocate %lu bytes of memory\n", st.st_size);
		return -1;
	}

	switch (test_vscsi_version (mem))
	{
		case VSCSI1:
			fprintf (stderr, "Detected VSCSI1 format.\n");
			fptr = print_trace_v1;
			delta = sizeof(trace_v1_record_t);
			break;

		case VSCSI2:
			fprintf (stderr, "Detected VSCSI2 format.\n");
			fptr = print_trace_v2;
			delta = sizeof(trace_v2_record_t);
			break;
		
		case UNKNOWN:
		default:
			fprintf (stderr, "Trace format unrecognized.\n");
			return -1;
	}

	while (done < st.st_size)
	{
		fptr (stdout, mem); 
		mem += delta;
		done += delta;
	}

	munmap (mem, st.st_size);

	close (f);

	return 0;
}
