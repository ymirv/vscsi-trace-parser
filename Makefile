CFLAGS=-m64 -Wall 
LFLAGS=
EXTRACT_OBJS=trace_format.o trace_extract.o
EXTRACT=trace_extract
SORT_OBJS=trace_format.o trace_sort.o
SORT=trace_sort

all: $(EXTRACT) $(SORT)

clean:
	rm $(EXTRACT) $(EXTRACT_OBJS) $(SORT) $(SORT_OBJS)

$(EXTRACT): $(EXTRACT_OBJS)
	gcc $(CFLAGS) $(LFLAGS) -o $@ $(EXTRACT_OBJS)

$(SORT): $(SORT_OBJS)
	gcc $(CFLAGS) $(LFLAGS) -o $@ $(SORT_OBJS)

.c.o:
	gcc $(CFLAGS) -c -o $@ $<
	
