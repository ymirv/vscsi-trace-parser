#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <inttypes.h>
#include <errno.h>
#include <string.h>
#include <assert.h>

#include "trace_format.h"


int comparatorV1 (const void * a, const  void * b)
{
    const trace_v1_record_t *t1 = (const trace_v1_record_t *) a;
    const trace_v1_record_t *t2 = (const trace_v1_record_t *) b;
    // n.b. not safe to return (t1->ts - t2->ts) because
    // the timestamps are of type uint64_t so the difference could
    // overflow int and become the opposite
    if (t1->ts < t2->ts) {
        return -1;
    } else if (t1->ts > t2->ts) {
        return 1;
    } else {
        return 0;
    }
}

int comparatorV2 (const void * a, const  void * b)
{
    const trace_v2_record_t *t1 = (const trace_v2_record_t *) a;
    const trace_v2_record_t *t2 = (const trace_v2_record_t *) b;
    if (t1->ts < t2->ts) {
        return -1;
    } else if (t1->ts > t2->ts) {
        return 1;
    } else {
        return 0;
    }
}


int main(int argc, char **argv)
{
    int ret = EXIT_SUCCESS;
    struct stat st;
    char *infile = NULL, *outfile = NULL;
    vscsi_version_t version;
    void *mem = NULL;
    int f = -1;
    size_t numrecords = 0;
    size_t filesize = 0;
    FILE *fin, *fout;

    if (argc < 3) {
        fprintf (stderr, "Usage: %s <tracefile> <outfile>\n", argv[0]);
        return 0;
    }

    infile = argv[1];
    outfile = argv[2];
    if ((f = open(infile, O_RDONLY)) == -1) {
        fprintf (stderr, "Unable to open '%s'\n", infile);
        return EXIT_FAILURE;
    }

    if ( (fin = fopen (infile,  "r")) == NULL) {
        fprintf (stderr, "Unable to open '%s'\n", infile);
        close(f);
        return EXIT_FAILURE;
    }
    fprintf(stderr, "Opened input file '%s' successfully\n", infile);

    if ((fout = fopen(outfile, "w")) == NULL) {
        fprintf(stderr, "Unable to open '%s'\n", outfile);
        close(f);
        fclose(fin);
        return EXIT_FAILURE;
    }

    if ((fstat (f, &st)) < 0) {
        fprintf (stderr, "Unable to fstat '%s'\n", infile);
        ret = EXIT_FAILURE;
        close(f);
        fclose(fin);
        fclose(fout);
        goto out;
    }

    filesize = st.st_size;
    printf("Filesize: %zu (%zuMB)\n", filesize, filesize >> 20);
    if ((mem = malloc(filesize)) == NULL) {
        fprintf(stderr, "Unable to malloc\n");
        return EXIT_FAILURE;
    }

    if (st.st_size < sizeof(trace_v2_record_t) * MAX_TEST) {
        fprintf (stderr, "File too small, unable to read header.\n");
        ret = EXIT_FAILURE;
        goto out;
    }

    if (fread(mem, filesize, 1, fin) != 1) {
        fprintf (stderr, "Failed to read input file into memory buffer.\n");
        ret = EXIT_FAILURE;
        goto out;
    }


    switch (test_vscsi_version (mem)) {
        case VSCSI1:
            version = VSCSI1;
            numrecords = filesize / sizeof(trace_v1_record_t);
            assert ((filesize % sizeof(trace_v1_record_t)) == 0);
            printf ("Number of records in input file: %zu\n", numrecords);
            fprintf (stderr, "Detected VSCSI1 format\n");
            break;

        case VSCSI2:
            version = VSCSI2;
            numrecords = filesize / sizeof(trace_v2_record_t);
            assert ((filesize % sizeof(trace_v2_record_t)) == 0);
            printf ("Number of records in input file: %zu\n", numrecords);
            fprintf (stderr, "Detected VSCSI2 format\n");
            break;

        case UNKNOWN:
        default:
            fprintf (stderr, "Trace format unrecognized\n");
            ret = EXIT_FAILURE;
            goto out;
    }

    if (version == VSCSI1) {
        printf("Performing VSCSI1 sort\n");
        qsort(mem, numrecords, sizeof(trace_v1_record_t), comparatorV1);
    } else if (version == VSCSI2) {
        printf("Performing VSCSI2 sort\n");
        qsort(mem, numrecords, sizeof(trace_v2_record_t), comparatorV2);
    }

    fprintf(stderr, "Records sorted successfully\n");

    if (fwrite(mem, filesize, 1, fout) != 1) {
        fprintf (stderr, "Failed to write to output file\n");
        ret = EXIT_FAILURE;
        goto out;
    }

    fprintf(stderr, "Wrote records successfully to output file '%s'\n", outfile);

out:
    free(mem);
    close (f);
    fclose (fin);
    fclose(fout);

    return ret;
}
